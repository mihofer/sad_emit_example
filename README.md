# sad_emit_example

Small repository to compare the emittances computed by SAD and by XSuite.

Lattices in SAD format are stored in the `lattices` directory, with the wigglers in SAD sequence already turned on (`BWIG[MP] Angle!=0`). 

The script to load the SAD lattice and check the emittance is wrapped in the function `run_sad` found in `toolkit/sad_helper.py`.
It also creates a MAD-X sequence, which is then converted for XSuite using the `convert_to_xsuite` function found in the same file.
Both functions create `.csv` files with the emittances.

In `test_emit.py`, the unit test are set up to run for a chosen operation-mode and compare the results of the emittances.
This is done by running `pytest ./tests/test_emit.py --operation-mode=[zwht] `.