from pathlib import Path
import pytest
import subprocess

import xtrack as xt
from cpymad.madx import Madx

SAD_COMMAND = """
ON ECHO;

READ "{lattice_path}";

FFS USE FCCEE_P_RING;
NPARA=8;
CONVERGENCE=10^-32;

MINCOUP=2e-3;

Get["{toolkit_path}/twiss.n"];

INS;
CALCULATE;


(* ----------------------PRINT TWISS----------------------------------------- *)
NORAD; ! Turn on radiation in all elements.
NOFLUC; ! Turn off quantum effects.
NORADCOD; ! calculate orbit considering radiation loss
NORADTAPER; ! automatic tapering of all magnets according to local energy of the closed orbit.
USE FCCEE_P_RING;
CELL;
CALCULATE;


SaveTwissFile["{cwd}/twiss_{operation_mode}_sad4d.tfs"];
(* ----------------------PRINT TWISS----------------------------------------- *)
RAD; ! Turn on radiation in all elements.
NOFLUC; ! Turn off quantum effects.
RADCOD; ! calculate orbit considering radiation loss
RADTAPER; ! automatic tapering of all magnets according to local energy of the closed orbit.
USE FCCEE_P_RING;
CELL;
CALCULATE;


SaveTwissFile["{cwd}/twiss_{operation_mode}_sad6d.tfs"];

(* ----------------------PRINT EMIT----------------------------------------- *)
EMIT;

emitdata = Emittance[Matrix->False, SaveEMIT->True];

Print[Emittances/.emitdata];

fn = OpenWrite['{cwd}/emittances_{operation_mode}_sad.csv'];
WriteString[fn, EMITX, ', ', EMITY, ', ', EMITZ];
Close[fn];


(* ----------------------CONVERT SAD SEQUENCES TO MAD SEQUENCE--------------- *)
Get["{toolkit_path}/SAD2MADX.n"];

mx=SAD2MADX[fname->"{cwd}/FCCee_{operation_mode}"];
mx@CreateMADX[];

Exit[];
"""

ENERGIES = {
    'z':45.6,
    'w':80,
    'h':120,
    't':182.5,
}

def run_sad(operation_mode, sad_executable, lattice_path, toolkit_path, cwd=Path('./')):

    with open(cwd/'check_emit.sad', 'w') as output_file:
        output_file.write(SAD_COMMAND.format(operation_mode=operation_mode,
                                             lattice_path=lattice_path,
                                             toolkit_path=toolkit_path,
                                             cwd=cwd))

    with open(cwd/'sad.log', 'w') as log_file:
        subprocess.run([sad_executable, 'check_emit.sad'], check=True, stdout=log_file, cwd=cwd)


def convert_to_xsuite(operation_mode, sequence_path, cwd=Path('./')):

    with Madx(stdout=None) as madx:
        madx.call(str(sequence_path))
        madx.input("""
        BWIGM : SBEND, ANGLE:= ANGLEBWIGM, L:= LBWIGM, TILT:= TILTBWIGM;
        BWIGP : SBEND, ANGLE:= ANGLEBWIGP, L:= LBWIGP, TILT:= TILTBWIGP;
""")
        madx.beam(particle='positron', pc=ENERGIES[operation_mode])
        madx.input('lagca1=0.; lagca2=0.;') # oddly, sad2madx doesnt seem to convert lag, tbc
        madx.use('fccee_p_ring')
        madx.twiss(file=str(cwd/f'twiss_{operation_mode}_madx.tfs'))
        madx.input('lagca1=0.4; lagca2=0.4;') # oddly, sad2madx doesnt seem to convert lag, tbc
        madx.use('fccee_p_ring')

        line_thick = xt.Line.from_madx_sequence(madx.sequence.fccee_p_ring, allow_thick=True,
                                        deferred_expressions=True)
        line_thick.particle_ref = xt.Particles(mass0=xt.ELECTRON_MASS_EV, p0c=ENERGIES[operation_mode]*10**9)
        
    line_thick.build_tracker()
    tw_thick_no_rad = line_thick.twiss(method='4d')
    
    line = line_thick.copy()
    Strategy = xt.slicing.Strategy
    Teapot = xt.slicing.Teapot
    slicing_strategies = [
        Strategy(slicing=Teapot(1)),  # Default catch-all as in MAD-X
        Strategy(slicing=Teapot(3), element_type=xt.Bend),
        Strategy(slicing=Teapot(3), element_type=xt.CombinedFunctionMagnet),
        # Strategy(slicing=Teapot(50), element_type=xt.Quadrupole), # Starting point
        Strategy(slicing=Teapot(5), name=r'^qf.*'),
        Strategy(slicing=Teapot(5), name=r'^qd.*'),
        Strategy(slicing=Teapot(5), name=r'^qfg.*'),
        Strategy(slicing=Teapot(5), name=r'^qdg.*'),
        Strategy(slicing=Teapot(5), name=r'^ql.*'),
        Strategy(slicing=Teapot(5), name=r'^qs.*'),
        Strategy(slicing=Teapot(10), name=r'^qb.*'),
        Strategy(slicing=Teapot(10), name=r'^qg.*'),
        Strategy(slicing=Teapot(10), name=r'^qh.*'),
        Strategy(slicing=Teapot(10), name=r'^qi.*'),
        Strategy(slicing=Teapot(10), name=r'^qr.*'),
        Strategy(slicing=Teapot(10), name=r'^qu.*'),
        Strategy(slicing=Teapot(10), name=r'^qy.*'),
        Strategy(slicing=Teapot(50), name=r'^qa.*'),
        Strategy(slicing=Teapot(50), name=r'^qc.*'),
        Strategy(slicing=Teapot(20), name=r'^sy\..*'),
    ]

    line.slice_thick_elements(slicing_strategies=slicing_strategies)
    line.build_tracker()
    tw_thin_before = line.twiss(ele_start=0, ele_stop=len(line)-1, method='4d',
                            twiss_init=tw_thick_no_rad.get_twiss_init(0))
        
    opt = line.match(
        only_markers=True,
        method='4d',
        ele_start=0, ele_stop=len(line)-1,
        twiss_init=tw_thick_no_rad.get_twiss_init(0),
        vary=xt.VaryList(['k1qf4', 'k1qf2', 'k1qd3', 'k1qd1',], step=1e-8,
        ),
        targets=[
            xt.TargetSet(at=xt.END, mux=tw_thick_no_rad.qx, muy=tw_thick_no_rad.qy, tol=1e-5),
        ]
    )
    opt.solve()
    tw_thin_no_rad = line.twiss(method='4d')
    tw_thin_no_rad.to_pandas().to_csv(cwd/f'twiss_{operation_mode}_xsuite4d.csv')
    line.to_json(cwd/f'FCCee_{operation_mode}.json')

    line.configure_radiation(model='mean')
    line.compensate_radiation_energy_loss()

    tw_rad_wig_on = line.twiss(method='6d', eneloss_and_damping=True)
    tw_rad_wig_on.to_pandas().to_csv(cwd/f'twiss_{operation_mode}_xsuite6d.csv')
    ex = tw_rad_wig_on.eq_gemitt_x
    ey = tw_rad_wig_on.eq_gemitt_y
    ez = tw_rad_wig_on.eq_gemitt_zeta

    with open(cwd/f'emittances_{operation_mode}_xsuite.csv', 'w') as result_file:
        result_file.write(f'{ex}, {ey}, {ez}\n')
    
    return ex, ey, ez