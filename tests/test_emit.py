
from pathlib import Path
import pytest
import pandas as pd
from numpy.testing import assert_allclose
import tfs

import tests.sad_helper as sad_helper

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
SAD_EXECUTABLE = REPOSITORY_TOP_LEVEL/'codes'/'SAD'/'bin'/'gs'


def test_compare_emit(_output_dir, operation_mode):

    xsuite_df = pd.read_csv(
        _output_dir/f'emittances_{operation_mode}_xsuite.csv',
        header=None,
        names=['ex', 'ey', 'ez']
        )
    sad_df = pd.read_csv(
        _output_dir/f'emittances_{operation_mode}_sad.csv',
        header=None,
        names=['ex', 'ey', 'ez']
        )

    assert_allclose(
        xsuite_df.to_numpy(),
        sad_df.to_numpy(),
        rtol=1e-2
    )


def test_compare_twiss(_output_dir, operation_mode):

    sad_df = tfs.read(_output_dir/f'twiss_{operation_mode}_sad6d.tfs', index='NAME')
    xsuite_df = pd.read_csv(_output_dir/f'twiss_{operation_mode}_xsuite6d.csv', index_col='name')
    xsuite_df.index = xsuite_df.index.str.upper()
    index_intersection = list(set(xsuite_df.index) & set(sad_df.index))

    assert len(index_intersection) > 0.

    assert_allclose(
        xsuite_df.loc[xsuite_df.index.isin(index_intersection), ['betx', 'bety']].to_numpy(),
        sad_df.loc[sad_df.index.isin(index_intersection), ['BETX', 'BETY']].to_numpy(),
        rtol=2e-2
    )

    assert_allclose(
        xsuite_df.loc[xsuite_df.index.isin(index_intersection), ['dx', 'dy']].to_numpy(),
        sad_df.loc[sad_df.index.isin(index_intersection), ['DX', 'DY']].to_numpy(),
        atol=1e-1
    )


@pytest.fixture(scope='module')
def _output_dir(operation_mode):

    tmp_path = Path(f'./{operation_mode}')
    tmp_path = tmp_path.resolve()
    tmp_path.mkdir(exist_ok=True)
    sad_helper.run_sad(
        operation_mode=operation_mode,
        sad_executable=SAD_EXECUTABLE,
        lattice_path=REPOSITORY_TOP_LEVEL/'lattices'/operation_mode/f'fccee_{operation_mode}.sad',
        toolkit_path=REPOSITORY_TOP_LEVEL/'toolkit',
        cwd=tmp_path
    )

    sad_helper.convert_to_xsuite(
        operation_mode=operation_mode,
        sequence_path=tmp_path/f'FCCee_{operation_mode}.seq',
        cwd=tmp_path
    )

    return tmp_path