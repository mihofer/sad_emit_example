import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--operation-mode", action="store", default="t", choices=['t','h','w','z'], help="Which operation-mode to test, Choices: 't','h','w','z' "
    )


@pytest.fixture(scope='module')
def operation_mode(request):
    return request.config.getoption("--operation-mode")